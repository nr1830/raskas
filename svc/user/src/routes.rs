use crate::{
    error, helper,
    models::{User, ValidateEmail},
};
use anyhow::Result;
use axum::{extract::State, http::StatusCode, Json};
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use utils::AppState;

// API ROUTES

pub async fn create_user(
    State(state): State<Arc<AppState>>,
    Json(payload): Json<User>,
) -> Result<(StatusCode, Json<User>), (StatusCode, Json<error::UserError>)> {
    // Get User Struct
    let user = User::create(payload.username, payload.password, payload.email);

    // Validate Fields
    let is_valid = user.validate_email().unwrap();

    // Handle Cases
    if is_valid {
        let insert_test = helper::add_user(&state.pool, &user).await;

        // Error Handling
        let res = match insert_test {
            Ok(res) => {
                tracing::info!("Success: {:#?}", res);
                Ok((StatusCode::CREATED, Json(user)))
            }
            Err(err) => {
                let error_message = error::UserError {
                    error: err.to_string(),
                };

                tracing::error!("Failed to write to database: {:#?}", err);
                Err((StatusCode::INTERNAL_SERVER_ERROR, Json(error_message)))
            }
        };

        res
    } else {
        let err = error::UserError {
            error: "Invalid Email".to_string(),
        };

        Err((StatusCode::INTERNAL_SERVER_ERROR, Json(err)))
    }
}

pub async fn login_as_user(
    State(state): State<Arc<AppState>>,
    Json(payload): Json<User>,
) -> Result<Json<LoginResult>, Json<error::UserError>> {
    // Validate / Sanitize Request

    // Decrypt Password

    // Check For User

    // Construct Response
    let resp = LoginResult {
        status: "Logged In".to_string(),
        message: "Success!".to_string(),
    };

    // TODO: Research how to assign a token to a user session
    //       for persistent access to a web application while
    //       their session is active.

    Ok(Json(resp))
}

#[derive(Deserialize, Serialize, Debug)]
pub struct LoginResult {
    status: String,
    message: String,
}

use anyhow::Result;
use bson::oid::ObjectId;
use log::{debug, info};
use regex::Regex;
use serde::{Deserialize, Serialize};

// USER DATA MODELS

#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    #[serde(rename = "_id")]
    pub id: Option<ObjectId>,
    pub email: String,
    pub username: String,
    pub password: String,
}

pub trait ValidateEmail {
    fn validate_email(&self) -> Result<bool>;
}

impl User {
    pub fn create(username: String, password: String, email: String) -> User {
        User {
            id: Some(bson::oid::ObjectId::new()),
            email,
            username,
            password,
        }
    }
}

impl ValidateEmail for User {
    fn validate_email(&self) -> Result<bool> {
        let email_regex = Regex::new(
            r"^([a-z0-9_+]([a-z0-9_+.]*[a-z0-9_+])?)@([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6})",
        )
        .unwrap();
        let is_valid = email_regex.is_match(&self.email);

        if is_valid {
            debug!("Email is valid!");
        } else {
            debug!("Email is NOT valid!");
        }

        Ok(is_valid)
    }
}

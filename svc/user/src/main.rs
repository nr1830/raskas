mod error;
mod helper;
mod models;
mod routes;

use axum::{
    routing::{get, post},
    Router,
};
use dotenv::dotenv;
use std::env;
use std::net::SocketAddr;
use std::sync::Arc;
use tower_http::cors::{Any, CorsLayer};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};
use utils::AppState;
use utils::DatabaseOptions;

#[tokio::main]
async fn main() {
    // Setup Env
    dotenv().ok();
    let db_uri = env::var("MONGO_URI").expect("MongoURI not set - exiting.");

    // Logging Init
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(
            std::env::var("RUST_LOG").unwrap_or_else(|_| "user=debug".into()),
        ))
        .with(tracing_subscriber::fmt::layer())
        .init();

    // Connect To Database
    let dbo = DatabaseOptions {
        uri: db_uri.to_string(),
        database: "users".to_string(),
    };

    let db_conn = dbo.init().await.unwrap();

    // Initialize Database
    let _index_res = helper::users_collection_init(&db_conn).await.unwrap();

    // Wrap Connection Into State
    let shared_state = Arc::new(AppState { pool: db_conn });

    // Web Server Setup
    let cors = CorsLayer::new().allow_origin(Any);
    let app = Router::new()
        .route("/", get(|| async { "Hello World - User API" }))
        .route(
            "/users",
            get(routes::login_as_user).post(routes::create_user),
        )
        .with_state(shared_state)
        .layer(cors);

    // Run
    serve(app, 3000).await;
}

// TODO: Change to 0.0.0.0 before containerizing
async fn serve(app: Router, port: u16) {
    let addr = SocketAddr::from(([127, 0, 0, 1], port));
    tracing::debug!("Listening on 127.0.0.1:{}", port);

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

use anyhow::Result;
use log::{debug, error, info, warn};
use serde::{Deserialize, Serialize};

// Generic API Error Wrapper
#[derive(Serialize, Deserialize, Debug)]
pub struct UserError {
    pub error: String,
}

pub fn handle_error<T: std::fmt::Debug>(result: Result<T>) {
    let handler = match result {
        Ok(res) => info!("Operation success: {:#?}", res),
        Err(error) => error!("Operation failure: {:#?}", error),
    };

    handler
}

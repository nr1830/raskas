use crate::models;

use anyhow::Result;
use bson::doc;
use futures::stream::TryStreamExt;
use log::{debug, error, info};
use mongodb::results::{CreateIndexResult, DeleteResult, InsertManyResult};
use utils::CollectionManager;

// USER ASSOCIATED HELPER FUNCTIONS

pub async fn users_collection_init(db: &mongodb::Database) -> Result<CreateIndexResult> {
    let mongo_api = CollectionManager::new(db.collection::<models::User>("users"));

    let index_options = mongodb::options::IndexOptions::builder()
        .unique(true)
        .build();
    let index_model = mongodb::IndexModel::builder()
        .keys(doc! { "email": 1u32 })
        .options(index_options)
        .build();

    info!("Attempting to create index on EMAIL field.");
    let index_result = mongo_api.create_index(index_model, None).await;
    let res = match index_result {
        Ok(res) => {
            info!("Success: {:#?}", res);

            Ok(res)
        }
        Err(err) => {
            error!("{:#?}", err);

            Err(err)
        }
    };

    res
}

pub async fn add_user(
    db: &mongodb::Database,
    user_data: &models::User,
) -> Result<InsertManyResult> {
    // TODO: Check for pre-existing user
    // TODO: Create function for handling cases:
    //          (1) user doesn't exist
    //          (2) user exists already
    // TODO: Set up unique indexes on collection

    let mongo_api = CollectionManager::new(db.collection::<models::User>("users"));
    let data = user_data;
    let users = vec![data];
    let insert_operation = mongo_api.create(users).await?;

    Ok(insert_operation)
}

async fn delete_user(db: &mongodb::Database, filter: &models::User) -> Result<DeleteResult> {
    let mongo_api = CollectionManager::new(db.collection::<models::User>("users"));
    let delete_result = mongo_api.delete_documents(filter, None).await?;

    Ok(delete_result)
}

async fn get_all_users(db: &mongodb::Database) -> Result<Vec<models::User>> {
    let mongo_api = CollectionManager::new(db.collection::<models::User>("users"));
    let mut cursor = mongo_api.get_document_cursor(None, None).await?;

    let mut users_vec: Vec<models::User> = Vec::new();
    while let Some(user) = cursor.try_next().await? {
        users_vec.push(models::User {
            id: user.id,
            email: user.email,
            username: user.username.to_string(),
            password: user.password.to_string(),
        })
    }

    Ok(users_vec)
}

use anyhow::Result;
use bson::{to_document, Document};
use log::{debug, error, info};
use mongodb::options::{ClientOptions, CreateIndexOptions, DeleteOptions, FindOptions};
use mongodb::results::{CreateIndexResult, DeleteResult, InsertManyResult};
use mongodb::IndexModel;
use mongodb::{Client, Collection};
use serde::Serialize;

pub struct AppState {
    pub pool: mongodb::Database,
}

pub struct DatabaseOptions {
    pub uri: String,
    pub database: String,
}

impl DatabaseOptions {
    pub async fn init(&self) -> Result<mongodb::Database> {
        info!("Initializing database connection.");

        let client_options = ClientOptions::parse(&self.uri).await?;
        let client = Client::with_options(client_options)?;
        let db = client.database(&self.database);

        Ok(db)
    }
}

pub fn handle_error<T: std::fmt::Debug>(result: Result<T>) {
    let _handler = match result {
        Ok(res) => info!("Operation success: {:#?}", res),
        Err(error) => error!("Operation failure: {:#?}", error),
    };
}

#[derive(Clone)]
pub struct CollectionManager<T> {
    collection: Collection<T>,
}

impl<T> CollectionManager<T> {
    // TODO: Create error handling for improper user input
    //          (1) Do you validate an email at the insert level or the struct
    //              creation level?
    //          (2) Should error handling be baked into the CollectionManager
    //              implementation, or elsewhere in the code? As it has to do with
    //              responding to the API, it wouldn't make that much sense putting
    //              it into my generic MongoDB Library I suppose ...

    pub fn new(collection: Collection<T>) -> CollectionManager<T> {
        CollectionManager { collection }
    }

    pub async fn create(&self, data: Vec<&T>) -> Result<InsertManyResult>
    where
        T: Serialize,
    {
        info!("Creating collection ...");
        let insert_result = self.collection.insert_many(data, None).await?;

        Ok(insert_result)
    }

    pub async fn get_document_cursor(
        &self,
        filter: Option<Document>,
        options: Option<FindOptions>,
    ) -> Result<mongodb::Cursor<T>> {
        let cursor = self.collection.find(filter, options).await?;

        Ok(cursor)
    }

    pub async fn create_index(
        &self,
        index: IndexModel,
        options: Option<CreateIndexOptions>,
    ) -> Result<CreateIndexResult> {
        let index_result = self.collection.create_index(index, options).await?;

        Ok(index_result)
    }

    pub async fn delete_documents(
        &self,
        query_data: &T,
        options: Option<DeleteOptions>,
    ) -> Result<DeleteResult>
    where
        T: Serialize,
    {
        let query = to_document(&query_data)?;

        let delete_result = self.collection.delete_many(query, options).await?;

        Ok(delete_result)
    }
}

import { useState } from 'react'
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <h1>Raskas - Storytelling Engine</h1>

      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
      </div>

      <div className="chatBox">
        <input onKeyDown={() => console.log("Message sent!")} name="usermsg" type="text" id="usermsg" />
      </div>

    </div>
  )
}

export default App

#!/bin/bash

# Project Operations Script
#
# This is for development purposes only. Do not place 
# non-test passwords into this script.

ENGINE=podman
MONGO_USERNAME=devTest
MONGO_PASSWORD=password123

### Run Database ###
MONGO_STATUS=$("${ENGINE}" ps | grep raskas-mongo)
if [[ "${MONGO_STATUS}" != "" ]]; then
  echo
  echo "Mongo is already running, ignoring database start-up."
  echo
else
  echo
  echo "Starting MongoDB"
  echo

  mkdir -p data
  "${ENGINE}" pull mongo:latest
  "${ENGINE}" run -d -p 27017:27017 \
    --name raskas-mongodb \
    -v /home/ano/access/project/raskas/data:/data/db \
    -e MONGO_INITDB_ROOT_USERNAME="${MONGO_USERNAME}" \
    -e MONGO_INITDB_ROOT_PASSWORD="${MONGO_PASSWORD}" \
    -e MONGO_INITDB_DATABASE="users" \
    mongo:latest

  echo
  echo "Mongodb Initialized ..."
  echo "Your username is: ${MONGO_USERNAME}"
  echo "Your password is: ${MONGO_PASSWORD}"
  echo

  "${ENGINE}" ps
fi

### Run Redis ###
# REDIS_STATUS=$("${ENGINE}" ps | grep raskas-redis)
# if [[ "${REDIS_STATUS}" != "" ]]; then
#   echo
#   echo "Redis is already running, ignoring redis start-up."
#   echo
# else
#   echo
#   echo "Starting Redis ..."
#   echo

#   "${ENGINE}" run -d \
#     --name raskas-redis \
#     -v redis:/usr/local/etc/redis \
#     redis \
#     redis-server /usr/local/etc/redis/redis.conf

#   echo
#   echo "Redis Initialized ..."
#   echo

#   "${ENGINE}" ps
# fi

### Build Application ###
#cargo build
#cd portal && npm install 

### Run Stack ###
